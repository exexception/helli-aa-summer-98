﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pin : MonoBehaviour
{
    [SerializeField] GameObject manager;
    private void OnTriggerEnter2D(Collider2D other)
    {
        
        Debug.Log("Game Over!");
        manager = GameObject.Find("GameManager");
        manager.GetComponent<GameManager>().State = GameManager.AppState.GAMEOVER;
    }
}
