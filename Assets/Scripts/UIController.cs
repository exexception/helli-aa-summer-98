﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class UIController : MonoBehaviour
{


    [SerializeField] GameManager manager;
    [SerializeField] TextMeshProUGUI levelMaxPin;
    [SerializeField] TextMeshProUGUI level;
    [SerializeField] Button resetButton;


    void Update()
    {
        ManageUITextUpdate();
        ResetButtonApearingManager();
    }

    private void ManageUITextUpdate()
    {
        if (manager.IsGameStateInPlay())
        {
            levelMaxPin.text = manager.GetRemindedPinsCount().ToString();
            level.text = "Level " + manager.GetLevel();
        }
    }

    private void ResetButtonApearingManager()
    {
        if (manager.IsGameFinished())
        {
            resetButton.gameObject.SetActive(true);
        }
    }
}
